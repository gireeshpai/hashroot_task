from .models import Blog
from .forms import WriteBlog

from django.contrib import admin
from django.contrib.auth.admin import   UserAdmin

# Register your models here.


class WriteABlog(UserAdmin):
    model = Blog
    add_form = WriteBlog


class BlogList(admin.ModelAdmin):
    list_display = ('user', 'publish_date', 'title')


admin.site.register(Blog, BlogList)
