from django.forms import ModelForm, Textarea
from .models import Blog


class WriteBlog(ModelForm):

    class Meta:
        model = Blog
        fields = ('title', 'content')
        widgets = {
            'content': Textarea(attrs={'cols': 60, 'rows': 10})
        }
    field_order = ['title', 'content']
