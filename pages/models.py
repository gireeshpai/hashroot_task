from django.db import models
from users.models import CustomUser
# Create your models here.


class Blog(models.Model):
    user = models.ForeignKey(CustomUser, on_delete=models.CASCADE)
    title = models.CharField(max_length=150, blank=False)
    publish_date = models.DateTimeField(auto_now_add=True)
    content = models.CharField(max_length=500, blank=False)

    def __str__(self):
        return self.title
