from users.models import CustomUser
from django.db.models.signals import post_save
from django.dispatch import receiver


@receiver(post_save, sender=CustomUser)
def saved_user_profile(sender, **kwargs):
    if kwargs.get('created'):
        print("Signal Called")
        print("Instance : ", kwargs.get('instance'))
        print("Created : ", kwargs.get('created'))
        CustomUser.objects.get_or_create(username=kwargs.get('instance'))
    else:
        print("Signal in else")
        print("Instance : ", kwargs.get('instance'))
        print("Created : ", kwargs.get('created'))
        CustomUser.objects.get_or_create(username=kwargs.get('instance'))
