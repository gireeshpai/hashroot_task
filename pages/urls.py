from django.urls import path

from . import views
from users.views import EditProfile

urlpatterns = [
    path('', views.HomePageView.as_view(), name='home'),
    path('edit_profile/', EditProfile.as_view(), name='edit_profile'),
    path('list_blog/', views.BlogListView.as_view(), name='list_blog'),
    path('write_blog/', views.WriteBlogView.as_view(), name='write_blog'),
]
