from django.urls import reverse_lazy
from django.views.generic import TemplateView, CreateView, ListView

from .models import Blog
from .forms import WriteBlog
# Create your views here.


class HomePageView(TemplateView):
    template_name = 'home.html'


class BlogListView(ListView):
    model = Blog
    queryset = Blog.objects.order_by('-publish_date')
    paginate_by = 10
    template_name = 'blog_list.html'


class WriteBlogView(CreateView):
    model = Blog
    form_class = WriteBlog
    template_name = 'write_blog.html'
    success_url = reverse_lazy('write_blog')

    def form_valid(self, form):
        form.instance.user = self.request.user
        return super(WriteBlogView, self).form_valid(form)
