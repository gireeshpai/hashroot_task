from django.db.models.signals import post_save

from django.urls import reverse_lazy, reverse
from django.views import generic

from .models import CustomUser
from .forms import CustomUserCreationForm,CustomUserChangeForm
# Create your views here.


class SignUp(generic.CreateView):
    form_class = CustomUserCreationForm
    success_url = reverse_lazy('login')
    template_name = 'signup.html'


class EditProfile(generic.UpdateView):
    model = CustomUser
    form_class = CustomUserChangeForm
    template_name = 'edit_profile.html'

    def get_object(self, queryset=None):
        return CustomUser.objects.get_or_create(username=self.request.user)[0]

    def get_success_url(self):
        return reverse('edit_profile')
